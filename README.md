# LIGO Synthetic PE

A Python library for rapidly generating synthetic parameter estimates (PE) for
compact binaries.

Built on top of the Fisher matrix code provided by [Shaon Ghosh's `EM_Bright`](https://github.com/shaonghosh/EM_Bright).


## Credits

Copyright (c) 2018 - Daniel Wysocki

Licensed under the MIT license
